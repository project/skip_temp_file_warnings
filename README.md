# Skip temp file warnings

Drupal deletes unused temporary managed files automatically in file_cron. 
However if the file has already been deleted from disk, then Drupal generates 
an error message:

The error message repeats on every cron cycle and there is no obvious way to 
recover from the UI.

It seems that this error message is unnecessary and unhelpful. This module 
will help to skip this warning.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/skip_temp_file_warnings).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/skip_temp_file_warnings).


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module configure the string at
   `admin -> config -> development -> logging and errors`.
1. Configure the string present in file URI.
    - Example: public is the string in the below log message
    - Could not delete temporary file "public://sample.jpg" during garbage 
      collection
1. Multiple types of file URI can also be configured by adding the strings in 
   comma separated values.
    - Example: public is the string in the below first log message.
    - youtube is the string in the second log message.
    - Could not delete temporary file "public://sample.jpg" during garbage 
      collection.
    - Could not delete temporary file "youtube://sample.mo4" during garbage 
      collection.
1. You can provide the values in comma separated like: public,youtube in case 
   of above multiple type of file uri errors.
